#ifndef BRNZ_H
#define BRNZ_H

#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include "rdythread.h"
#include <QScrollArea>
#include <QFileDialog>
#include <QThread>

namespace Ui {
class brnz;
}

class brnz : public QMainWindow
{
    Q_OBJECT

public:
    explicit brnz(QWidget *parent = 0);
    ~brnz();

signals:

    void repRec();

    void sigRM();

    void doNext();

    void snd();

    void rcv();

private slots:

//    void open();

    void on_SendInfButton_clicked();

    void on_MeasButtonOff_clicked();

    void receive();

    void SlotRdy();

    void on_openButton_clicked();

    void showRM();

    void on_MeasButtonOn_clicked();

    //void showSend();

    //void showReceive();

    //void on_sendSave_clicked();

    //void on_sendReceive_clicked();

    void on_sendProgDataButton_clicked();

    void on_sendProgInqButton_clicked();

    void on_saveButton_clicked();

    void on_readButton_clicked();

private:
    Ui::brnz *ui;
    QSerialPort *serial;
    void initSerial();
    void deinitSerial();
    unsigned char byte;
    QByteArray sendDat, recDat, nextWord, wordRM;
    QByteArray copyRM1, copyRM2, copyRM3, copyRM4, copyRM5, copyRM6, copyRM7, copyRM8;
    QVector <unsigned char> recMas;
    QTimer *waitMes, *RdyTimer;
    double distRM, temp, press;
    QString str, filepathSend, filepathRec;
    int amountRM, sizeNext;
    QThread *Rdy;
    RdyThread *RdyTrd;
    QString path, filepath;
    bool signRdy;
    bool signMeas[8];
    bool signInfBort;
    bool signInfLand;
    const QString pathDebug;
};

#endif // BRNZ_H
