#include "rdythread.h"
#include <QTimer>

//constructor
RdyThread::RdyThread()
{
    rdyTimer = new QTimer(this);
    connect(rdyTimer, SIGNAL(timeout()), this, SLOT(sendToMainThread()));
}

RdyThread::~RdyThread()
{
    delete rdyTimer;
}

void RdyThread::waitRdy()
{
    rdyTimer->start(100);
}

void RdyThread::sendToMainThread()
{
    rdyTimer->stop();
    emit finished();
}
