#ifndef RDYTHREAD_H
#define RDYTHREAD_H

#include <QObject>
#include <QTimer>

class RdyThread : public QObject
{
    Q_OBJECT
public:
    RdyThread();
    ~RdyThread();

public slots:
    void waitRdy();
    void sendToMainThread();

signals:
    void finished();

private:
    QTimer *rdyTimer;
};

#endif // RDYTHREAD_H
