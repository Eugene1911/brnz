#-------------------------------------------------
#
# Project created by QtCreator 2015-12-17T13:48:33
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = BRNZ
TEMPLATE = app


SOURCES += main.cpp\
        brnz.cpp \
    rdythread.cpp

HEADERS  += brnz.h \
    rdythread.h

FORMS    += brnz.ui
