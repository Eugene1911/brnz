#include "brnz.h"
#include "ui_brnz.h"
#include <QFile>
#include <QTimer>
#include <QString>
#include <QDebug>
#include "rdythread.h"
#include <QThread>
#include <QFileDialog>
#include <QTime>

brnz::brnz(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::brnz),
    pathDebug(qApp->applicationDirPath() + "/debug.txt")
{
    ui->setupUi(this);
//    waitMes = new QTimer(this);
    RdyTimer = new QTimer(this);
    amountRM = 0;

//    connect (waitMes, SIGNAL(timeout()), this, SLOT(receive()));
//    connect (RdyTimer, SIGNAL(timeout()), this, SLOT(SlotRdy()));
    connect (this, SIGNAL(sigRM()), this, SLOT(showRM()));
    connect (this, SIGNAL(doNext()), this, SLOT(receive()));
    Rdy = new QThread;
    RdyTrd = new RdyThread;
    RdyTrd->moveToThread(Rdy);
    connect (Rdy, SIGNAL(started()), RdyTrd, SLOT(waitRdy()));
    connect (RdyTrd, SIGNAL(finished()), Rdy, SLOT(quit()));
    connect (RdyTrd, SIGNAL(finished()), this, SLOT(SlotRdy()));
    //connect (Rdy, SIGNAL(finished()), RdyTrd, SLOT(deleteLater()));
    //connect (RdyTrd, SIGNAL(finished()), Rdy, SLOT(deleteLater()));

    path = "C//";
    signRdy = false;
    this->initSerial();
}

brnz::~brnz()
{
    this->deinitSerial();
    delete ui;
}

void brnz::initSerial()
{
    serial = new QSerialPort;
    connect(this->serial, SIGNAL(readyRead()), SLOT(receive()));
}

void brnz::deinitSerial()
{
    if (serial->isOpen())
        serial->close();
    delete serial;
}

void brnz::on_SendInfButton_clicked()
{
    sendDat.clear();
    double codX[3]={0,0,0};
    double codY[3]={0,0,0};
    double codHigh[3]={0,0,0};
    double coord;
    byte = 3;
    sendDat.append(byte);
    coord = ui->XSpinBox->value();
    if (coord>=16384){
        codX[2] += 4;
        coord -= 16384;
    }
    if (coord>=8192){
        codX[2] += 2;
        coord -= 8192;
    }
    if (coord>=4096){
        codX[1] += 128;
        coord -= 4096;
    }
    if (coord>=2048){
        codX[1] += 64;
        coord -= 2048;
    }
    if (coord>=1024){
        codX[1] += 32;
        coord -= 1024;
    }
    if (coord>=512){
        codX[1] += 16;
        coord -= 512;
    }
    if (coord>=256){
        codX[1] += 8;
        coord -= 256;
    }
    if (coord>=128){
        codX[1] += 4;
        coord -= 128;
    }
    if (coord>=64){
        codX[1] += 2;
        coord -= 64;
    }
    if (coord>=32){
        codX[0] += 128;
        coord -= 32;
    }
    if (coord>=16){
        codX[0] += 64;
        coord -= 16;
    }
    if (coord>=8){
        codX[0] += 32;
        coord -= 8;
    }
    if (coord>=4){
        codX[0] += 16;
        coord -= 4;
    }
    if (coord>=2){
        codX[0] += 8;
        coord -= 2;
    }
    if (coord>=1){
        codX[0] += 4;
        coord -= 1;
    }
    if (coord>=.5){
        codX[0] += 2;
        coord = 0;
    }
    byte = codX[0];
    sendDat.append(byte);
    byte = codX[1];
    sendDat.append(byte);
    byte = codX[2];
    sendDat.append(byte);
    coord = ui->YSpinBox->value();
    if (coord>=16384){
        codY[2] += 4;
        coord -= 16384;
    }
    if (coord>=8192){
        codY[2] += 2;
        coord -= 8192;
    }
    if (coord>=4096){
        codY[1] += 128;
        coord -= 4096;
    }
    if (coord>=2048){
        codY[1] += 64;
        coord -= 2048;
    }
    if (coord>=1024){
        codY[1] += 32;
        coord -= 1024;
    }
    if (coord>=512){
        codY[1] += 16;
        coord -= 512;
    }
    if (coord>=256){
        codY[1] += 8;
        coord -= 256;
    }
    if (coord>=128){
        codY[1] += 4;
        coord -= 128;
    }
    if (coord>=64){
        codY[1] += 2;
        coord -= 64;
    }
    if (coord>=32){
        codY[0] += 128;
        coord -= 32;
    }
    if (coord>=16){
        codY[0] += 64;
        coord -= 16;
    }
    if (coord>=8){
        codY[0] += 32;
        coord -= 8;
    }
    if (coord>=4){
        codY[0] += 16;
        coord -= 4;
    }
    if (coord>=2){
        codY[0] += 8;
        coord -= 2;
    }
    if (coord>=1){
        codY[0] += 4;
        coord -= 1;
    }
    if (coord>=.5){
        codY[0] += 2;
        coord = 0;
    }
    byte = codY[0];
    sendDat.append(byte);
    byte = codY[1];
    sendDat.append(byte);
    byte = codY[2];
    sendDat.append(byte);
    coord = ui->HighSpinBox->value();
    if (coord>=3276.8){
        codHigh[2] += 4;
        coord -= 3276.8;
    }
    if (coord>=1638.4){
        codHigh[2] += 2;
        coord -= 1638.4;
    }
    if (coord>=819.2){
        codHigh[1] += 128;
        coord -= 819.2;
    }
    if (coord>=409.6){
        codHigh[1] += 64;
        coord -= 409.6;
    }
    if (coord>=204.8){
        codHigh[1] += 32;
        coord -= 204.8;
    }
    if (coord>=102.4){
        codHigh[1] += 16;
        coord -= 102.4;
    }
    if (coord>=51.2){
        codHigh[1] += 8;
        coord -= 51.2;
    }
    if (coord>=25.6){
        codHigh[1] += 4;
        coord -= 25.6;
    }
    if (coord>=12.8){
        codHigh[1] += 2;
        coord -= 12.8;
    }
    if (coord>=6.4){
        codHigh[0] += 128;
        coord -= 6.4;
    }
    if (coord>=3.2){
        codHigh[0] += 64;
        coord -= 3.2;
    }
    if (coord>=1.6){
        codHigh[0] += 32;
        coord -= 1.6;
    }
    if (coord>=.8){
        codHigh[0] += 16;
        coord -= .8;
    }
    if (coord>=.4){
        codHigh[0] += 8;
        coord -= .4;
    }
    if (coord>=.2){
        codHigh[0] += 4;
        coord -= .2;
    }
    if (coord>=.1){
        codHigh[0] += 2;
        coord -= .1;
    }
    byte = codHigh[0];
    sendDat.append(byte);
    byte = codHigh[1];
    sendDat.append(byte);
    byte = codHigh[2];
    sendDat.append(byte);
    byte = 0;
    sendDat.append(byte);
    sendDat.append(byte);
    sendDat.append(byte);
    serial->write(sendDat);
}

void brnz::on_MeasButtonOff_clicked()
{
    qDebug() << "measButtonOff";
    sendDat.clear();
    byte = 1;
    sendDat.append(byte);
    serial->write(sendDat);
    //emit snd();
    return;
}

void brnz::receive()
{
    int i, j, numB, cod, numBar, ver;
    bool endMes;
    endMes = false;
    qDebug() <<"nextWord.size =" << nextWord.size();
    if (nextWord.size()>0){
        for (i=0; i<=nextWord.size()-1; i++)
            recDat.append(nextWord[i]);
        nextWord.clear();
    }
    recDat.append(serial->readAll());
    qDebug() <<"recDat.size =" << recDat.size();
    numB = recDat.size();
    recMas.clear();
    for (i=0; i<recDat.size(); i++)
        recMas.append(recDat[i]);
    qDebug() <<"recMas.size =" << recMas.size();
    while (recDat[0]%2 == 0){
        if (recDat.size()>=1){
            recDat.remove(0,1);
            recMas.remove(0,1);
        }
        else{
            recDat.clear();
            recMas.clear();
            return;
        }
    }
    qDebug() <<"proverka na zagolovok"<<recDat.size();
    if (recDat.size()>=1){
        cod = recMas[0] & 14;
        cod = cod >> 1;
        qDebug() <<"cod =" << cod;
    }
    else return;
    if (recDat.size()>1){
        if (cod>1){
            while (recDat[1]%2 == 1){//проверка на новый заголовок
                if (recDat.size()>1){
                    recDat.remove(0, 1);
                    recMas.remove(0, 1);
                }
                else return;
            }
        }
    }
    qDebug() <<"2_recDat.size =" << recDat.size();
    qDebug() <<"2_recMas.size =" << recMas.size();


    //если пришло больше одного сообщения, то следующуе слово записываем в nextWord, а recDat обрезаем до размеров сообщения
    if (cod == 0){//готовность
        if (recDat.size() == 1)
            endMes = true;
        else if (recDat.size()>1){
            for (i=1; i<=recDat.size()-1; i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            recDat.append(recMas[0]);
            endMes = true;
        }
    }
    if (cod == 4){//данные измерения
        if (recDat.size() == 5)
            endMes = true;
        else if (recDat.size()>5){
            for (i=5; i<=recDat.size()-1; i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            for (i=0; i<=4; i++)
                recDat.append(recMas[i]);
            endMes = true;
        }
        else if (recDat.size()<5)
            return;
    }
    if (cod == 2){//данные барометров
        if (recDat.size() == 7)
            endMes = true;
        else if (recDat.size()>7){
            for (i=7; i<=recDat.size()-1; i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            for (i=0; i<=6; i++)
                recDat.append(recMas[i]);
            endMes = true;
        }
        else if (recDat.size()<7)
            return;
    }
    if (cod == 6){//температура РЭМ
        if (recDat.size() == 3)
            endMes = true;
        else if (recDat.size()>3){
            for (i=3; i<=recDat.size()-1; i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            for (i=0; i<=2; i++)
                recDat.append(recMas[i]);
            endMes = true;
        }
        else if (recDat.size()<3)
            return;
    }
    if (cod == 1){//запрос полетной информации
        if (recDat.size() == 1)
            endMes = true;
        else if (recDat.size()>1){
            for (i=1; i<=recDat.size()-1; i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            recDat.append(recMas[0]);
            endMes = true;
        }
    }
    if (cod == 7){//запрограммированные данные
        if (recDat.size() == 2)
            endMes = true;
        else if (recDat.size()>2){
            for (i=2; i<recDat.size(); i++)
                nextWord.append(recDat[i]);
            recDat.clear();
            for (i=0; i<=1; i++)
                recDat.append(recMas[i]);
            endMes = true;
        }
        else if (recDat.size()<2)
            return;
    }
    if (endMes == true){//расшифровка
        if (cod == 0){//код пакета "готовность"
            ui->RdyLabel->setStyleSheet("background-color: green");
            signRdy = true;
            Rdy->start();
            //RdyTimer->start(100);
            recDat.clear();
            recMas.clear();
        }
        if (cod == 4){//код пакета "данные измерения"
            if (recDat.size() == 5){
                for (i=0; i<=4; i++)
                    wordRM.append(recMas[i]);
                int numRM;
                numRM = wordRM[0] & 112;
                numRM = numRM >> 4;
                if (amountRM == 1){
                    if (numRM == 0)//РМ1
                        copyRM1 = wordRM;
                    if (numRM == 1)//РМ2
                        copyRM2 = wordRM;
                    if (numRM == 2)//РМ3
                        copyRM3 = wordRM;
                    if (numRM == 3)//РМ4
                        copyRM4 = wordRM;
                    if (numRM == 4)//РМ5
                        copyRM5 = wordRM;
                    if (numRM == 5)//РМ6
                        copyRM6 = wordRM;
                    if (numRM == 6)//РМ7
                        copyRM7 = wordRM;
                    if (numRM == 7)//РМ8
                        copyRM8 = wordRM;
                }
                if (numRM == 7)
                    amountRM++;
                if (amountRM == 3)
                    emit sigRM();
                else if (amountRM == 100)
                    amountRM = 0;

                recDat.clear();
                recMas.clear();
                wordRM.clear();
            }
        }
        if (cod == 2){//код пакета "данные барометров"
            if (recDat.size() == 7){
                numBar = recMas[0] &112;
                numBar = numBar >> 4;
                if (numBar == 0){//барометр "Борт"
                    if (recMas[0]>128){
                        ui->InfBort->setStyleSheet("background-color: green");
                        signInfBort = true;
                    }
                    else{
                        ui->InfBort->setStyleSheet("background-color: red");
                        signInfBort = false;
                    }
                    press = 0;
                    temp = 0;
                    byte = recMas[1];
                    if (byte >= 128){
                        press += 10.48;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        press += 5.24;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 2.62;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 1.31;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 0.655;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 0.3275;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 0.16375;
                        byte -= 2;
                    }
                    byte = recMas[2];
                    if (byte >= 128){
                        press += 1341.44;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        press += 670.62;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 335.36;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 167.68;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 83.84;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 41.92;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 20.96;
                        byte -= 2;
                    }
                    byte = recMas[3];
                    if (byte >= 64){
                        press += 85852.16;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 42926.08;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 21463.04;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 10731.52;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 5365.76;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 2682.88;
                        byte -= 2;
                    }
                    str = QString::number(press);
                    for (int i=0;i<=str.size()-1;i++)
                        if (str[i]=='.')
                            str[i]=',';
                    ui->PressBort->setText(str);
                    byte = recMas[4];
                    if (byte >= 128){
                        temp += .08;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        temp += .04;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        temp += .02;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        temp += .01;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += .005;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += .0025;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp +=.00125;
                        byte -= 2;
                    }
                    byte = recMas[5];
                    if (byte >= 128){
                        temp += 10.24;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        temp += 5.12;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        temp += 2.56;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        temp += 1.28;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += .64;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += .32;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp += .16;
                        byte -= 2;
                    }
                    byte = recMas[6];
                    if (byte >= 16){
                        temp += 163.84;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += 81.92;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += 40.96;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp += 20.48;
                        byte -= 2;
                    }
                    str = QString::number(temp);
                    for (int i=0;i<=str.size()-1;i++)
                        if (str[i]=='.')
                            str[i]=',';
                    ui->TempBort->setText(str);
                }
                if (numBar == 4){//барометр "Земля"
                    if (recMas[0]>128){
                        ui->InfLand->setStyleSheet("background-color: green");
                        signInfLand = true;
                    }
                    else{
                        ui->InfLand->setStyleSheet("background-color: red");
                        signInfLand = false;
                    }
                    press = 0;
                    temp = 0;
                    byte = recMas[1];
                    if (byte >= 128){
                        press += 10.48;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        press += 5.24;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 2.62;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 1.31;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 0.655;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 0.3275;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 0.16375;
                        byte -= 2;
                    }
                    byte = recMas[2];
                    if (byte >= 128){
                        press += 1341.44;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        press += 670.62;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 335.36;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 167.68;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 83.84;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 41.92;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 20.96;
                        byte -= 2;
                    }
                    byte = recMas[3];
                    if (byte >= 64){
                        press += 85852.16;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        press += 42926.08;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        press += 21463.04;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        press += 10731.52;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        press += 5365.76;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        press += 2682.88;
                        byte -= 2;
                    }
                    str = QString::number(press);
                    for (int i=0;i<=str.size()-1;i++)
                        if (str[i]=='.')
                            str[i]=',';
                    ui->PressLand->setText(str);
                    byte = recMas[4];
                    if (byte >= 128){
                        temp += .08;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        temp += .04;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        temp += .02;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        temp += .01;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += .005;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += .0025;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp +=.00125;
                        byte -= 2;
                    }
                    byte = recMas[5];
                    if (byte >= 128){
                        temp += 10.24;
                        byte -= 128;
                    }
                    if (byte >= 64){
                        temp += 5.12;
                        byte -= 64;
                    }
                    if (byte >= 32){
                        temp += 2.56;
                        byte -= 32;
                    }
                    if (byte >= 16){
                        temp += 1.28;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += .64;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += .32;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp += .16;
                        byte -= 2;
                    }
                    byte = recMas[6];
                    if (byte >= 16){
                        temp += 163.84;
                        byte -= 16;
                    }
                    if (byte >= 8){
                        temp += 81.92;
                        byte -= 8;
                    }
                    if (byte >= 4){
                        temp += 40.96;
                        byte -= 4;
                    }
                    if (byte >= 2){
                        temp += 20.48;
                        byte -= 2;
                    }
                    str = QString::number(temp);
                    for (int i=0;i<=str.size()-1;i++)
                        if (str[i]=='.')
                            str[i]=',';
                    ui->TempLand->setText(str);
                }
                recDat.clear();
                recMas.clear();
            }
        }
        if (cod == 6){//код пакета "температура РЭМ"
            if (recDat.size() == 3){
                temp = recMas[1] >> 1;
                ver = recMas[2] >> 4;
                byte = recMas[2] & 2;
                if (byte == 2)
                    temp += 128;
                str = QString::number(temp);
                for (int i=0;i<=str.size()-1;i++)
                    if (str[i]=='.')
                        str[i]=',';
                ui->tempREMbox->setText(str);
                str = QString::number(ver);
                for (int i=0;i<=str.size()-1;i++)
                    if (str[i]=='.')
                        str[i]=',';
                ui->verREMbox->setText(str);
                recDat.clear();
                recMas.clear();
            }
        }       
        if (cod == 1){//запрос полетной информации
            //this->on_SendInfButton_clicked();
            recDat.clear();
            recMas.clear();
        }
        if (cod == 7){//запрограммированные данные

            //log
            QFile file(pathDebug);
            if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
                return;
            QTextStream out(&file);
            QString write = recDat.toHex();
            QTime time;
            time = QTime::currentTime();
            QString strTime = time.toString("hh.mm.ss");
            out << "programm data: " << write << '\n' << strTime << '\n';
            file.close();

            switch (recDat[1] & 6){
            case 0:
                ui->Chan60_2->setChecked(true);
                break;
            case 2:
                ui->Chan70_2->setChecked(true);
                break;
            case 4:
                ui->Chan80_2->setChecked(true);
                break;
            case 6:
                ui->Chan90_2->setChecked(true);
                break;
            default:
                ui->erLabel->setText("uncorrected message 'запрограммированные данные'");
                break;
            }
            switch ((recDat[1] & 128)>>7){
            case 0:
                ui->AttOff_2->setChecked(true);
                break;
            case 1:
                ui->AttOn_2->setChecked(true);
                break;
            }
            recDat.clear();
            recMas.clear();
        }
    }
    if (nextWord.size()>0){
        //recMas.clear();
        emit doNext();
    }
    recDat.clear();
    recMas.clear();
    return;
}

void brnz::SlotRdy()
{
    qDebug() <<"rdy: whiteSmoke";
    ui->RdyLabel->setStyleSheet("background-color: WhiteSmoke");
    signRdy = false;
    Rdy->quit();
    //RdyTrd->finished();
    //RdyTimer->stop();
    return;
}

void brnz::on_openButton_clicked()
{
    if (this->serial->isOpen()){
        serial->close();
        ui->openButton->setText("Открыть порт");
    }
    else{//конфигурация порта
        QString com, baud, datBits, parity, stops;
        int bd;
        QFile file("portImi.cfg");
        QString str;
        if(file.open(QIODevice::ReadOnly |QIODevice::Text)){
            int n = 0;
            while (!file.atEnd()){
                str = file.readLine();
                if (n == 0)
                    com = str;
                if (n == 1)
                    datBits = str;
                if (n == 2)
                    parity = str;
                if (n == 3)
                    stops = str;
                if (n == 4)
                    baud = str;
                n += 1;
            }
        }
        com = com.left(com.indexOf('\n'));
        bd = baud.toInt();
        serial->setPortName(com);
        if (serial->open(QSerialPort::ReadWrite)){
            serial->setBaudRate(bd);
            if (datBits == "5\n")
                serial->setDataBits(QSerialPort::Data5);
            if (datBits == "6\n")
                serial->setDataBits(QSerialPort::Data6);
            if (datBits == "7\n")
                serial->setDataBits(QSerialPort::Data7);
            if (datBits == "8\n")
                serial->setDataBits(QSerialPort::Data8);
            if (parity == "Even\n"){
                //serial->setParity(QSerialPort::EvenParity);
                serial->setParity(QSerialPort::EvenParity);
            }
            if (parity == "Odd\n"){
                serial->setParity(QSerialPort::OddParity);
            }
            if (parity == "No\n"){
                serial->setParity(QSerialPort::NoParity);
            }
            if (stops == "1")
                serial->setStopBits(QSerialPort::OneStop);
            if (stops == "2")
                serial->setStopBits(QSerialPort::TwoStop);
            serial->setFlowControl(QSerialPort::NoFlowControl);
            ui->openButton->setText("закрыть");
        }
        else{
            ui->erLabel->setText("Error opened serial device");
        }
    }
    QFile fileP("path.cfg");
    QTextStream input(&fileP);
    input.setCodec("Windows-1251");
    while (!input.atEnd()){
        path = input.readLine();
    }
}

void brnz::showRM()
{
    unsigned char att;
    QString attStr;
    //РМ1
    byte = copyRM1[0];
    if (byte>=128){
        ui->Meas1->setStyleSheet("background-color: green");
        signMeas[0] = true;
    }
    else{
        ui->Meas1->setStyleSheet("background-color: red");
        signMeas[0] = false;
    }
    if (signMeas[0]==true){
        distRM = 0;
        byte = copyRM1[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM1[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM1[3];
        if (byte>=128){
            ui->MeasAcc1->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc1->setText("низкая");
        }
        if (byte>=64){
            ui->signEr1->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr1->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist1->setText(str);
        att = copyRM1[4];
        if ((copyRM1[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_1->setText(attStr);
    }
    //РМ2
    byte = copyRM2[0];
    if (byte>=128){
        ui->Meas2->setStyleSheet("background-color: green");
        signMeas[1] = true;
    }
    else{
        ui->Meas2->setStyleSheet("background-color: red");
        signMeas[1] = false;
    }
    if (signMeas[1]==true){
        distRM = 0;
        byte = copyRM2[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM2[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM2[3];
        if (byte>=128){
            ui->MeasAcc2->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc2->setText("низкая");
        }
        if (byte>=64){
            ui->signEr2->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr2->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist2->setText(QString("%1").arg(str));
        att = copyRM2[4];
        if ((copyRM2[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_2->setText(attStr);
    }
    //РМ3
    byte = copyRM3[0];
    if (byte>=128){
        ui->Meas3->setStyleSheet("background-color: green");
        signMeas[2] = true;
    }
    else{
        ui->Meas3->setStyleSheet("background-color: red");
        signMeas[2] = false;
    }
    if (signMeas[2]==true){
        distRM = 0;
        byte = copyRM3[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM3[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM3[3];
        if (byte>=128){
            ui->MeasAcc3->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc3->setText("низкая");
        }
        if (byte>=64){
            ui->signEr3->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr3->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist3->setText(QString("%1").arg(str));
        att = copyRM3[4];
        if ((copyRM3[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_3->setText(attStr);
    }
    //РМ4
    byte = copyRM4[0];
    if (byte>=128){
        ui->Meas4->setStyleSheet("background-color: green");
        signMeas[3] = true;
    }
    else{
        ui->Meas4->setStyleSheet("background-color: red");
        signMeas[3] = false;
    }
    if (signMeas[3]==true){
        distRM = 0;
        byte = copyRM4[1];
        if (byte>=128){
        distRM += 19.2;
        byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM4[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM4[3];
        if (byte>=128){
            ui->MeasAcc4->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc4->setText("низкая");
        }
        if (byte>=64){
            ui->signEr4->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr4->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist4->setText(QString("%1").arg(str));
        att = copyRM4[4];
        if ((copyRM4[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_4->setText(attStr);
    }
    //РМ5
    byte = copyRM5[0];
    if (byte>=128){
        ui->Meas5->setStyleSheet("background-color: green");
        signMeas[4] = true;
    }
    else{
        ui->Meas5->setStyleSheet("background-color: red");
        signMeas[4] = false;
    }
    if (signMeas[4]==true){
        distRM = 0;
        byte = copyRM5[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM5[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM5[3];
        if (byte>=128){
            ui->MeasAcc5->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc5->setText("низкая");
        }
        if (byte>=64){
            ui->signEr5->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr5->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist5->setText(QString("%1").arg(str));
        att = copyRM5[4];
        if ((copyRM5[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_5->setText(attStr);
   }
    //РМ6
    byte = copyRM6[0];
    if (byte>=128){
        ui->Meas6->setStyleSheet("background-color: green");
        signMeas[5] = true;
    }
    else{
        ui->Meas6->setStyleSheet("background-color: red");
        signMeas[5] = false;
    }
    if (signMeas[5]==true){
        distRM = 0;
        byte = copyRM6[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM6[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM6[3];
        if (byte>=128){
           ui->MeasAcc6->setText("высокая");
            byte -= 128;
        }
        else{
           ui->MeasAcc6->setText("низкая");
        }
        if (byte>=64){
            ui->signEr6->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr6->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist6->setText(QString("%1").arg(str));
        att = copyRM6[4];
        if ((copyRM6[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_6->setText(attStr);
    }
    //РМ7
    byte = copyRM7[0];
    if (byte>=128){
        ui->Meas7->setStyleSheet("background-color: green");
        signMeas[6] = true;
    }
    else{
        ui->Meas7->setStyleSheet("background-color: red");
        signMeas[6] = false;
    }
    if (signMeas[6]==true){
        distRM = 0;
        byte = copyRM7[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM7[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM7[3];
        if (byte>=128){
            ui->MeasAcc7->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc7->setText("низкая");
        }
        if (byte>=64){
            ui->signEr7->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr7->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist7->setText(QString("%1").arg(str));
        att = copyRM7[4];
        if ((copyRM7[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_7->setText(attStr);
    }
    //РМ8
    byte = copyRM8[0];
    if (byte>=128){
        ui->Meas8->setStyleSheet("background-color: green");
        signMeas[7] = true;
    }
    else{
        ui->Meas8->setStyleSheet("background-color: red");
        signMeas[7] = false;
    }
    if (signMeas[7]==true){
        distRM = 0;
        byte = copyRM8[1];
        if (byte>=128){
            distRM += 19.2;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 9.6;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 4.8;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 2.4;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 1.2;
            byte -= 8;
        }
        if (byte>=4){
            distRM += .6;
            byte -= 4;
        }
        if (byte>=2){
            distRM += .3;
            byte -= 2;
        }
        byte = copyRM8[2];
        if (byte>=128){
            distRM += 2457.6;
            byte -= 128;
        }
        if (byte>=64){
            distRM += 1228.8;
            byte -= 64;
        }
        if (byte>=32){
            distRM += 614.4;
            byte -= 32;
        }
        if (byte>=16){
            distRM += 307.2;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 153.6;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 76.8;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 38.4;
            byte -= 2;
        }
        byte = copyRM8[3];
        if (byte>=128){
            ui->MeasAcc8->setText("высокая");
            byte -= 128;
        }
        else{
            ui->MeasAcc8->setText("низкая");
        }
        if (byte>=64){
            ui->signEr8->setText("+");
            byte -= 64;
        }
        else{
            ui->signEr8->setText("-");
        }
        if (byte>=32)
            byte -= 32;
        if (byte>=16){
            distRM += 39321.6;
            byte -= 16;
        }
        if (byte>=8){
            distRM += 19660.8;
            byte -= 8;
        }
        if (byte>=4){
            distRM += 9830.4;
            byte -= 4;
        }
        if (byte>=2){
            distRM += 4915.2;
            byte -= 2;
        }
        str = QString::number(distRM);
        for (int i=0;i<=str.size()-1;i++)
            if (str[i]=='.')
                str[i]=',';
        ui->dist8->setText(QString("%1").arg(str));
        att = copyRM8[4];
        if ((copyRM8[3] & 32)==32)
            att += 1;
        attStr.setNum(att);
        ui->attState_8->setText(attStr);
    }

    qDebug() <<"copyRM1.size =" << copyRM1.size();
    qDebug() <<"copyRM2.size =" << copyRM2.size();
    qDebug() <<"copyRM3.size =" << copyRM3.size();
    qDebug() <<"copyRM4.size =" << copyRM4.size();
    qDebug() <<"copyRM5.size =" << copyRM5.size();
    qDebug() <<"copyRM6.size =" << copyRM6.size();
    qDebug() <<"copyRM7.size =" << copyRM7.size();
    qDebug() <<"copyRM8.size =" << copyRM8.size();
}

void brnz::on_MeasButtonOn_clicked()
{
    sendDat.clear();
    byte = 129;
    sendDat.append(byte);
    serial->write(sendDat);
    QFile file(pathDebug);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        return;
    QTextStream out(&file);
    QString write = sendDat.toHex();
    QTime time;
    time = QTime::currentTime();
    QString strTime = time.toString("hh.mm.ss");
    out << "measure: " << write << '\n' << strTime << '\n';
    file.close();
    //emit snd();
    return;
}

void brnz::on_sendProgDataButton_clicked()
{
    sendDat.clear();
    byte = 15;
    sendDat.append(15);
    byte = 0;
    if (ui->Chan70->isChecked())
        byte += 2;
    else if (ui->Chan80->isChecked())
        byte += 4;
    else if (ui->Chan90->isChecked())
        byte += 6;
    if (ui->AttOn->isChecked())
        byte += 128;
    sendDat.append(byte);
    serial->write(sendDat);
}

void brnz::on_sendProgInqButton_clicked()
{
    ui->Chan60_2->setAutoExclusive(false);
    ui->Chan70_2->setAutoExclusive(false);
    ui->Chan80_2->setAutoExclusive(false);
    ui->Chan90_2->setAutoExclusive(false);
    ui->AttOff_2->setAutoExclusive(false);
    ui->AttOn_2->setAutoExclusive(false);

    ui->Chan60_2->setChecked(false);
    ui->Chan70_2->setChecked(false);
    ui->Chan80_2->setChecked(false);
    ui->Chan90_2->setChecked(false);
    ui->AttOff_2->setChecked(false);
    ui->AttOn_2->setChecked(false);

    ui->Chan60_2->setAutoExclusive(true);
    ui->Chan70_2->setAutoExclusive(true);
    ui->Chan80_2->setAutoExclusive(true);
    ui->Chan90_2->setAutoExclusive(true);
    ui->AttOff_2->setAutoExclusive(true);
    ui->AttOn_2->setAutoExclusive(true);
    sendDat.clear();
    byte = 7;
    sendDat.append(byte);
    serial->write(sendDat);
    QFile file(pathDebug);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Append))
        return;
    QTextStream out(&file);
    QString write = sendDat.toHex();
    QTime time;
    time = QTime::currentTime();
    QString strTime = time.toString("hh.mm.ss");
    out << "inquiry: " << write << '\n' << strTime << '\n';
    file.close();
}

void brnz::on_saveButton_clicked()
{
    filepath = path;
    filepath.append("\\");
    QTime time;
    time = QTime::currentTime();
    QString strTimeCom = time.toString("hh.mm.ss");
    filepath.append(strTimeCom);
    filepath.append(".sav");
    QString filename;
    filename = QFileDialog::getSaveFileName(
                this,
                tr("open file"),
                filepath,
                "Sav files (*.sav);;"
                );
    filepath = filename;
    int ind = filename.lastIndexOf('/');
    for (int i=filename.size(); i>ind; i--)
        filepath = filepath.remove(i, 1);
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return;
    QTextStream out(&file);
    QString write;
    write.setNum(ui->XSpinBox->value());
    out << write << "\n";
    write.setNum(ui->YSpinBox->value());
    out << write << "\n";
    write.setNum(ui->HighSpinBox->value());
    out << write << "\n";
    out << ui->PressBort->text() << "\n";
    out << ui->TempBort->text() << "\n";
    if (signInfBort==true)
        write = "background-color: green";
    else
        write = "background-color: red";
    out << write << "\n";
    out << ui->PressLand->text() << "\n";
    out << ui->TempLand->text() << "\n";
    if (signInfLand==true)
        write = "background-color: green";
    else
        write = "background-color: red";
    out << write << "\n";
    if (ui->Chan60->isChecked())
        write = "1160";
    else if (ui->Chan70->isChecked())
        write  = "1170";
    else if (ui->Chan80->isChecked())
        write  = "1180";
    else if (ui->Chan90->isChecked())
        write  = "1190";
    out << write << "\n";
    if (ui->AttOn->isChecked())
        write = "on";
    else if (ui->AttOff->isChecked())
        write = "off";
    out << write << "\n";
    out << ui->tempREMbox->text() << "\n";
    out << ui->verREMbox->text() << "\n";
    if (signRdy==true)
        write = "background-color: green";
    else
        write = "background-color: WhiteSmoke";
    out << write << "\n";

    out << ui->dist1->text() << "\n";
    out << ui->attState_1->text() << "\n";
    out << ui->signEr1->text() << "\n";
    out << ui->MeasAcc1->text() << "\n";
    if (signMeas[0]==true)
        write = "background-color: green";
    else if (signMeas[0]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist2->text() << "\n";
    out << ui->attState_2->text() << "\n";
    out << ui->signEr2->text() << "\n";
    out << ui->MeasAcc2->text() << "\n";
    if (signMeas[1]==true)
        write = "background-color: green";
    else if (signMeas[1]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist3->text() << "\n";
    out << ui->attState_3->text() << "\n";
    out << ui->signEr3->text() << "\n";
    out << ui->MeasAcc3->text() << "\n";
    if (signMeas[2]==true)
        write = "background-color: green";
    else if (signMeas[2]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist4->text() << "\n";
    out << ui->attState_4->text() << "\n";
    out << ui->signEr4->text() << "\n";
    out << ui->MeasAcc4->text() << "\n";
    if (signMeas[3]==true)
        write = "background-color: green";
    else if (signMeas[3]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist5->text() << "\n";
    out << ui->attState_5->text() << "\n";
    out << ui->signEr5->text() << "\n";
    out << ui->MeasAcc5->text() << "\n";
    if (signMeas[4]==true)
        write = "background-color: green";
    else if (signMeas[4]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist6->text() << "\n";
    out << ui->attState_6->text() << "\n";
    out << ui->signEr6->text() << "\n";
    out << ui->MeasAcc6->text() << "\n";
    if (signMeas[5]==true)
        write = "background-color: green";
    else if (signMeas[5]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist7->text() << "\n";
    out << ui->attState_7->text() << "\n";
    out << ui->signEr7->text() << "\n";
    out << ui->MeasAcc7->text() << "\n";
    if (signMeas[6]==true)
        write = "background-color: green";
    else if (signMeas[6]==false)
        write = "background-color: red";
    out << write << "\n";

    out << ui->dist8->text() << "\n";
    out << ui->attState_8->text() << "\n";
    out << ui->signEr8->text() << "\n";
    out << ui->MeasAcc8->text() << "\n";
    if (signMeas[7]==true)
        write = "background-color: green";
    else if (signMeas[7]==false)
        write = "background-color: red";
    out << write << "\n";
    file.close();
}

void brnz::on_readButton_clicked()
{
    if (filepath=="")
        filepath = "C://";
    QString filename = QFileDialog::getOpenFileName(
                this,
                tr("open file"),
                filepath,
                "Sav files (*.sav);;"
                );
    filepath = filename;
    int ind = filename.lastIndexOf('/');
    for (int i=filename.size(); i>ind; i--)
        filepath = filepath.remove(i, 1);
    QFile file(filename);

    file.open(QIODevice::ReadOnly);
    QTextStream stream(&file);
    QString read;
    int stroka = 0;
    while (!stream.atEnd()){
        read = stream.readLine();
        switch (stroka) {
        case 0:{
            double x = read.toDouble();
            ui->XSpinBox->setValue(x);
        }break;
        case 1:{
            double y = read.toDouble();
            ui->YSpinBox->setValue(y);
        }break;
        case 2:{
            double h = read.toDouble();
            ui->HighSpinBox->setValue(h);
        }break;
        case 3:
            ui->PressBort->setText(read);
            break;
        case 4:
            ui->TempBort->setText(read);
            break;
        case 5:
            ui->InfBort->setStyleSheet(read);
            break;
        case 6:
            ui->PressLand->setText(read);
            break;
        case 7:
            ui->TempLand->setText(read);
            break;
        case 8:
            ui->InfLand->setStyleSheet(read);
            break;
        case 9:
            if (read=="1160")
                ui->Chan60->setChecked(true);
            else if (read=="1170")
                ui->Chan70->setChecked(true);
            else if (read=="1180")
                ui->Chan80->setChecked(true);
            else if (read=="1190")
                ui->Chan90->setChecked(true);
            break;
        case 10:
            if (read=="on")
                ui->AttOn->setChecked(true);
            else
                ui->AttOff->setChecked(true);
            break;
        case 11:
            ui->tempREMbox->setText(read);
            break;
        case 12:
            ui->verREMbox->setText(read);
            break;
        case 13:
            ui->RdyLabel->setStyleSheet(read);
            break;
        case 14:
            ui->dist1->setText(read);
            break;
        case 15:
            ui->attState_1->setText(read);
            break;
        case 16:
            ui->signEr1->setText(read);
            break;
        case 17:
            ui->MeasAcc1->setText(read);
            break;
        case 18:
            ui->Meas1->setStyleSheet(read);
            break;
        case 19:
            ui->dist2->setText(read);
            break;
        case 20:
            ui->attState_2->setText(read);
            break;
        case 21:
            ui->signEr2->setText(read);
            break;
        case 22:
            ui->MeasAcc2->setText(read);
            break;
        case 23:
            ui->Meas2->setStyleSheet(read);
            break;
        case 24:
            ui->dist3->setText(read);
            break;
        case 25:
            ui->attState_3->setText(read);
            break;
        case 26:
            ui->signEr3->setText(read);
            break;
        case 27:
            ui->MeasAcc3->setText(read);
            break;
        case 28:
            ui->Meas3->setStyleSheet(read);
            break;
        case 29:
            ui->dist4->setText(read);
            break;
        case 30:
            ui->attState_4->setText(read);
            break;
        case 31:
            ui->signEr4->setText(read);
            break;
        case 32:
            ui->MeasAcc4->setText(read);
            break;
        case 33:
            ui->Meas4->setStyleSheet(read);
            break;
        case 34:
            ui->dist5->setText(read);
            break;
        case 35:
            ui->attState_5->setText(read);
            break;
        case 36:
            ui->signEr5->setText(read);
            break;
        case 37:
            ui->MeasAcc5->setText(read);
            break;
        case 38:
            ui->Meas5->setStyleSheet(read);
            break;
        case 39:
            ui->dist6->setText(read);
            break;
        case 40:
            ui->attState_6->setText(read);
            break;
        case 41:
            ui->signEr6->setText(read);
            break;
        case 42:
            ui->MeasAcc6->setText(read);
            break;
        case 43:
            ui->Meas6->setStyleSheet(read);
            break;
        case 44:
            ui->dist7->setText(read);
            break;
        case 45:
            ui->attState_7->setText(read);
            break;
        case 46:
            ui->signEr7->setText(read);
            break;
        case 47:
            ui->MeasAcc7->setText(read);
            break;
        case 48:
            ui->Meas7->setStyleSheet(read);
            break;
        case 49:
            ui->dist8->setText(read);
            break;
        case 50:
            ui->attState_8->setText(read);
            break;
        case 51:
            ui->signEr8->setText(read);
            break;
        case 52:
            ui->MeasAcc8->setText(read);
            break;
        case 53:
            ui->Meas8->setStyleSheet(read);
            break;
        default:
            break;
        }
        stroka++;
    }
    file.close();
}
